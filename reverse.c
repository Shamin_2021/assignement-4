#include <stdio.h>

int main() {
  
  int numero,reverse=0,remainder;
  printf("enter the number ");
  scanf("%d",&numero);

  while(numero!=0){
    remainder = numero%10 ;
    reverse = reverse*10 + remainder;
    numero /=10;
  }
  
  printf("reverse number is : %d ",reverse );
  
  return 0;
}
