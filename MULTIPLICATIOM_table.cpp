#include <iostream>
using namespace std;

int main() {
  
  int n;
  cout << "enter the number : ";
  cin >>n;

  for (int i=1;i<=n; i++)
  {
    cout << "----multiplication table for " << i <<"----"<<endl;
    for(int j=1; j <=12 ;j++){
      cout << i <<"x" << j << "=" << i*j <<endl; 
    }
    cout <<endl;
    
  }
}
